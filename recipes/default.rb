#
# Cookbook:: test
# Recipe:: default
#
# Copyright:: 2017, The Authors, All Rights Reserved.

node.default['chef_client']['logrotate']['frequency'] = 'hourly'
node.default['chef_client']['config']['verify_api_cert'] = false
node.default['chef_client']['config']['ssl_verify_mode'] = :verify_none
node.default['chef_client']['config']['log_level'] = :info
node.default['chef_client']['interval'] = 300
node.default['chef_client']['splay'] = 30

include_recipe 'chef-client::config'
include_recipe 'chef-client::default'
include_recipe 'ssh-hardening::default'

hab_install 'install habitat'

file '/root/testing' do
  content Chef::Config[:foo]
end

node.default['audit']['interval']['enabled'] = true
node.default['audit']['reporter'] = 'chef-server-automate'
node.default['audit']['fetcher'] = 'chef-server'
node.default['audit']['insecure'] = true
#node.default['audit']['inspec_version'] = '2.2.10'
#node.default['audit']['inspec_version'] = '2.1.59'

node.default['audit']['profiles'] = [
  {
    name: 'DevSec SSH Baseline',
    compliance: 'delivery/ssh-baseline',
  # }, {
  #   name: 'DevSec Linux Baseline',
  #   compliance: 'delivery/linux-baseline',
  # }, {
  #   name: 'DevSec Linux Patch Baseline',
  #   compliance: 'delivery/linux-patch-baseline',
  },

]

include_recipe 'audit::default'
